const express = require('express')
const {MongoClient, ObjectId} = require('mongodb')
const bodyParser = require('body-parser')
const app = express()
const port = 3001
const client = new MongoClient('mongodb://localhost:27017/')
const MONGO_ID_REGEX = /^([^\W_]{12}){1,2}$/

const jsonParser = bodyParser.json()

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/connection', async (req, res) => {
  try {
    // Connect the client to the server
    await client.connect();
    // Establish and verify connection
    await client.db("admin").command({ ping: 1 });
    console.log("Connected successfully to server");
  } catch(e){
    console.log("Connection failed")
  }
    finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
})

app.get('/todos', async (req, res) => {
  try{
    await client.connect();
    const db = client.db('todo')
    const todos = db.collection('todos')
    const todoList = await todos.find().toArray()
    res.send(todoList)
  } catch(e){
    console.log(e)
  } finally{
    await client.close()
  }
})

app.get('/todos/:id', async (req, res, next) => {
  const {id} = req.params
  try{
    const _id = new ObjectId(req.params.id)
    await client.connect()
    const db = client.db('todo')
    const todos = db.collection('todos')
    const todo = await todos.findOne({_id})
    res.send(todo)
  } catch(e){
    console.log(e)
  }
})

app.post('/addTodo', jsonParser, async (req, res) => {
  try{
    await client.connect();
    const db = client.db('todo')
    const todos = db.collection('todos')

    const _id = await todos.insertOne({task: req.body.task, state: req.body.state})
    const result = {_id: _id, task: req.body.task, state: req.body.state}
    res.send(result)
  } catch(e){
    console.log(e)
    res.sendStatus(404)
  } finally{
    await client.close()
  }
})


app.put('/todos/task/:id', jsonParser, async (req, res, next) => {
  const {id} = req.params
  try{
    const {task} = req.body
    const _id = new ObjectId(req.params.id)
    await client.connect()
    const db = client.db('todo')
    const todos = db.collection('todos')
    const {lastErrorObject, value: todo} = await todos.findOneAndUpdate({_id}, {
      $set : {
        task
      }
    })
    if(lastErrorObject.n){
      res.send(todo)
    } else {
      res.status(400).send({error :  "not find"})
    }
  
  } catch(e){
    console.log(e)
  }
})


app.put('/todos/state/:id', jsonParser, async (req, res, next) => {
  const {id} = req.params
  try{
    const {state} = req.body
    const _id = new ObjectId(req.params.id)
    await client.connect()
    const db = client.db('todo')
    const todos = db.collection('todos')
    const {lastErrorObject, value: todo} = await todos.findOneAndUpdate({_id}, {
      $set : {
        state
      }
    })
    if(lastErrorObject.n){
      res.send(todo)
    } else {
      res.status(400).send({error :  "not find"})
    }
  
  } catch(e){
    console.log(e)
  }
})

app.delete('/todos/:id', async (req, res, next) => {
  const {id} = req.params
  try{
      const _id = new ObjectId(id)
      await client.connect();
      const db = client.db("todo");
      const todos = db.collection("todos");
      const todo = await todos.deleteOne({_id});
      res.send(todo);
      
    }catch(e){
          console.log(e);
    }finally{
        client.close()
    }
})

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})
